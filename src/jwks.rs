use actix::fut::{ready, Ready};
use actix_web::{
    dev::{Service, ServiceRequest, ServiceResponse, Transform},
    http::{header, StatusCode},
    HttpMessage,
};
use futures_util::Future;
use josekit::{
    jwk::{Jwk, JwkSet},
    jws::RS256,
    jwt::{self, JwtPayloadValidator},
};
use parking_lot::RwLock;
use std::{pin::Pin, rc::Rc, time::SystemTime};

use crate::error::MiaoError;

pub struct JwksClient {
    url: String,
    jwk_set: RwLock<JwkSet>,
}

impl JwksClient {
    pub fn new<U: Into<String>>(url: U) -> Self {
        Self {
            url: url.into(),
            jwk_set: RwLock::new(JwkSet::new()),
        }
    }

    pub async fn get(&self, input: &str) -> Result<Jwk, MiaoError> {
        let header = match jwt::decode_header(input) {
            Ok(header) => header,
            _ => {
                return Err(MiaoError::InternalServerError(
                    "Failed to decode jwt header".to_owned(),
                ))
            }
        };

        let key_id = match header.claim("kid").and_then(|key_id| key_id.as_str()) {
            Some(key_id) => key_id,
            _ => {
                return Err(MiaoError::InternalServerError(
                    "Key id is missing from jwt header".to_owned(),
                ))
            }
        };

        let alg = match header.claim("alg").and_then(|key_id| key_id.as_str()) {
            Some(alg) => alg,
            _ => {
                return Err(MiaoError::InternalServerError(
                    "alg is missing from jwt header".to_owned(),
                ))
            }
        };

        {
            let jwk_set = self.jwk_set.read();

            for jwk in jwk_set.get(key_id.to_string().as_ref()) {
                if jwk.algorithm().unwrap_or("") == alg {
                    return Ok(jwk.clone());
                }
            }
        }

        let fetched_jwk_set = self.fetch_keys().await?;

        for jwk in fetched_jwk_set.get(key_id) {
            if jwk.algorithm().unwrap_or("") != alg {
                continue;
            }

            {
                let mut jwk_set = self.jwk_set.write();
                *jwk_set = fetched_jwk_set.clone();
            }

            return Ok(jwk.clone());
        }

        Err(MiaoError::InternalServerError("Jwk not found".to_owned()))
    }

    async fn fetch_keys(&self) -> Result<JwkSet, MiaoError> {
        let client = awc::Client::default();

        let req = client.get(&self.url);
        let mut res = req.send().await.unwrap();
        let body = match res.status() {
            StatusCode::OK => res.body().await.unwrap(),
            _ => {
                return Err(MiaoError::InternalServerError(format!(
                    "Failed to fetch {} {}",
                    res.status(),
                    self.url
                )))
            }
        };

        Ok(JwkSet::from_bytes(&body)?)
    }
}

pub struct JwksActix(pub String);

impl<S, B> Transform<S, ServiceRequest> for JwksActix
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error> + 'static,
    B: 'static,
    S::Future: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    type Transform = JwksActixMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(JwksActixMiddleware {
            service: Rc::new(service),
            jwks_client: Rc::new(JwksClient::new(&self.0)),
        }))
    }
}

type BoxFuture<O> = Pin<Box<dyn Future<Output = O>>>;

pub struct JwksActixMiddleware<S> {
    service: Rc<S>,
    jwks_client: Rc<JwksClient>,
}

impl<S, B> Service<ServiceRequest> for JwksActixMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error> + 'static,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    type Future = BoxFuture<Result<Self::Response, Self::Error>>;

    actix_web::dev::forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let service = Rc::clone(&self.service);
        let jwks_client = Rc::clone(&self.jwks_client);

        Box::pin(async move {
            let token = match req
                .headers()
                .get(header::AUTHORIZATION)
                .and_then(|v| v.to_str().ok())
            {
                Some(value) => value.replace("Bearer ", ""),
                _ => return service.call(req).await,
            };

            let jwk = jwks_client.get(&token).await?;
            let verifier = RS256.verifier_from_jwk(&jwk).map_err(MiaoError::from)?;
            let (payload, _) =
                jwt::decode_with_verifier(&token, &verifier).map_err(MiaoError::from)?;

            let mut validator = JwtPayloadValidator::new();
            validator.set_base_time(SystemTime::now());

            if validator.validate(&payload).is_ok() {
                req.extensions_mut().insert(payload);
            }

            service.call(req).await
        })
    }
}
