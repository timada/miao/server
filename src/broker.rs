use actix::Message;
use pulsar::{producer, DeserializeMessage, Error, Payload, SerializeMessage};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Message, Clone)]
#[rtype(result = "()")]
pub struct BrokerMessage {
    pub topic_name: String,
    pub account_id: Option<String>,
    pub event_type: String,
    pub data: serde_json::Value,
    pub metadata: Option<serde_json::Value>,
}

impl<'a> SerializeMessage for &'a BrokerMessage {
    fn serialize_message(input: Self) -> Result<producer::Message, Error> {
        let payload = serde_json::to_vec(input).map_err(|e| Error::Custom(e.to_string()))?;
        Ok(producer::Message {
            payload,
            ..Default::default()
        })
    }
}

impl DeserializeMessage for BrokerMessage {
    type Output = Result<BrokerMessage, serde_json::Error>;

    fn deserialize_message(payload: &Payload) -> Self::Output {
        serde_json::from_slice(&payload.data)
    }
}
