mod api;

pub mod jwks;
pub mod broker;
pub mod error;
pub mod settings;
pub mod topic_filter;
pub mod topic_name;
pub mod ws_server;
pub mod ws_session;

use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc, Mutex,
};

use actix::*;
use jwks::JwksActix;
use actix_web::{
    web::{self, Data},
    App, HttpServer, Responder,
};
use error::MiaoActix;
use pulsar::{producer, proto, Authentication, Pulsar, TokioExecutor};
use settings::SETTINGS;
use tracing_subscriber::EnvFilter;
use ws_server::WsServer;

#[macro_use]
extern crate tracing;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate nanoid;

///  Displays and affects state
async fn get_count(count: web::Data<Arc<AtomicUsize>>) -> impl Responder {
    let current_count = count.load(Ordering::SeqCst);
    format!("Visitors: {}", current_count)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    if let Some(rust_log) = &SETTINGS.log {
        std::env::set_var("RUST_LOG", rust_log);
    }

    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    let mut builder = Pulsar::builder(SETTINGS.broker.url.to_owned(), TokioExecutor);
    let pulsar_auth = SETTINGS
        .pulsar
        .as_ref()
        .and_then(|pulsar| pulsar.authentication.as_ref());

    if let Some(pulsar_auth) = pulsar_auth {
        let authentication = Authentication {
            name: pulsar_auth.name.to_owned(),
            data: pulsar_auth.data.to_owned().into_bytes(),
        };

        builder = builder.with_auth(authentication);
    }

    let pulsar = builder
        .build()
        .await
        .expect("Failed to create pulsar builder");

    let producer = pulsar
        .producer()
        .with_topic(SETTINGS.broker.topic.to_owned())
        .with_name(nanoid!())
        .with_options(producer::ProducerOptions {
            schema: Some(proto::Schema {
                r#type: proto::schema::Type::String as i32,
                ..Default::default()
            }),
            ..Default::default()
        })
        .build()
        .await
        .map(|producer| Arc::new(Mutex::new(producer)))
        .expect("Failed to build producer");

    let app_state = Arc::new(AtomicUsize::new(0));
    let server = WsServer::new(app_state.clone(), pulsar.clone()).start();

    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(app_state.clone()))
            .app_data(Data::new(server.clone()))
            .app_data(Data::new(producer.clone()))
            .route("/count", web::get().to(get_count))
            .wrap(MiaoActix())
            .wrap(JwksActix(SETTINGS.jwks_url.to_owned()))
            .service(api::scope())
            .service(web::resource("/").to(ws_server::route))
    })
    .bind(("0.0.0.0", SETTINGS.port.unwrap_or(6750)))?
    .run()
    .await
}
