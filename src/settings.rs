use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;
#[derive(Debug, Deserialize)]
pub struct PulsarAuth {
    pub name: String,
    pub data: String,
}

#[derive(Debug, Deserialize)]
pub struct Pulsar {
    pub authentication: Option<PulsarAuth>,
}

#[derive(Debug, Deserialize)]
pub struct Broker {
    pub url: String,
    pub topic: String,
}

#[derive(Debug, Deserialize)]
pub struct Jwt {
    pub key: String,
    pub expires_at: u64,
    pub issuer: String,
    pub audience: String,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub port: Option<u16>,
    pub jwt: Jwt,
    pub jwks_url: String,
    pub broker: Broker,
    pub pulsar: Option<Pulsar>,
    pub log: Option<String>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let s = Config::builder()
            .add_source(File::with_name("miao").required(false))
            .add_source(File::with_name("miao-local").required(false))
            .add_source(Environment::with_prefix("miao"))
            .build()?;

        s.try_deserialize()
    }
}

lazy_static! {
    pub static ref SETTINGS: Settings = match Settings::new() {
        Ok(s) => s,
        Err(e) => panic!("Application settings: {e}"),
    };
}
