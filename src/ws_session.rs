//! `WsServer` is an actor. It maintains list of connection client session.
//! And manages available rooms. Peers send messages to other peers in same
//! room through `WsServer`.

use actix::prelude::*;
use actix_web_actors::ws::{self, ProtocolError, WebsocketContext};
use std::collections::HashSet;
use std::ops::Add;
use std::time::{Duration, Instant, SystemTime};

use josekit::{
    jws::{JwsHeader, HS256},
    jwt::{self, JwtPayload},
};

use crate::ws_server::{Connect, Disconnect, Subscribe, WsServer};

use super::settings::SETTINGS;

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

#[derive(Message)]
#[rtype(result = "()")]
pub enum Message {
    Account(Subscribe),
    Text(String),
}

pub struct WsSession {
    pub id: String,
    pub hb: Instant,
    pub addr: Addr<WsServer>,
    pub account_ids: HashSet<String>,
}

impl Actor for WsSession {
    type Context = WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);

        let addr = ctx.address();
        self.addr
            .send(Connect {
                addr: addr.recipient(),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(res) => act.id = res,
                    _ => ctx.stop(),
                }
                fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        self.addr.do_send(Disconnect {
            id: self.id.to_owned(),
            account_ids: self.account_ids.to_owned(),
        });
        Running::Stop
    }
}

impl Handler<Message> for WsSession {
    type Result = ();

    fn handle(&mut self, msg: Message, ctx: &mut Self::Context) {
        match msg {
            Message::Account(sub) => {
                self.account_ids.insert(sub.account_id.to_owned());
                self.addr.do_send(sub)
            }
            Message::Text(text) => {
                ctx.text(text);
            }
        };
    }
}

impl StreamHandler<Result<ws::Message, ProtocolError>> for WsSession {
    fn handle(&mut self, msg: Result<ws::Message, ProtocolError>, ctx: &mut Self::Context) {
        let msg = match msg {
            Err(_) => {
                ctx.stop();
                return;
            }
            Ok(msg) => msg,
        };

        match msg {
            ws::Message::Ping(msg) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            }
            ws::Message::Pong(_) => {
                self.hb = Instant::now();
            }
            ws::Message::Text(text) => {
                debug!("WEBSOCKET MESSAGE: {:?}", text);

                let m = text.trim();
                if !m.starts_with('/') {
                    ctx.text(format!("!!! unknown command: {:?}", m))
                }

                let v: Vec<&str> = m.splitn(2, ' ').collect();

                match v[0] {
                    "/token" => {
                        let mut header = JwsHeader::new();
                        header.set_token_type("JWT");

                        let mut payload = JwtPayload::new();
                        payload.set_subject(self.id.to_owned());
                        payload.set_expires_at(
                            &SystemTime::now().add(Duration::from_secs(SETTINGS.jwt.expires_at)),
                        );
                        payload.set_audience(SETTINGS.jwt.audience.split(',').collect());
                        payload.set_issuer(SETTINGS.jwt.issuer.to_owned());

                        let res = HS256
                            .signer_from_bytes(&SETTINGS.jwt.key)
                            .and_then(|signer| jwt::encode_with_signer(&payload, &header, &signer));

                        match res {
                            Ok(jwt) => ctx.text(format!("token {jwt}")),
                            Err(e) => error!("websocket.StreamHandler.handle /token => {e}"),
                        };
                    }
                    _ => ctx.text(format!("!!! unknown command: {:?}", m)),
                }
            }
            ws::Message::Binary(_) => error!("Unexpected binary"),
            ws::Message::Close(reason) => {
                ctx.close(reason);
                ctx.stop();
            }
            ws::Message::Continuation(_) => {
                ctx.stop();
            }
            ws::Message::Nop => (),
        }
    }
}

impl WsSession {
    fn hb(&self, ctx: &mut WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                debug!("Websocket Client heartbeat failed, disconnecting!");

                act.addr.do_send(Disconnect {
                    id: act.id.to_owned(),
                    account_ids: act.account_ids.to_owned(),
                });

                ctx.stop();

                return;
            }

            ctx.ping(b"");
        });
    }
}
