use actix::prelude::*;
use actix_web::{web, Error, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use futures_util::TryStreamExt;
use nanoid::nanoid;
use pulsar::{Consumer, Pulsar, SubType, TokioExecutor};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::time::Instant;

use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc,
};

use crate::broker::BrokerMessage;
use crate::topic_filter::{TopicFilter, TopicFilterError};
use crate::topic_name::TopicNameRef;
use crate::ws_session::{Message, WsSession};

use super::settings::SETTINGS;

#[derive(Message)]
#[rtype(String)]
pub struct Connect {
    pub addr: Recipient<Message>,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Disconnect {
    pub id: String,
    pub account_ids: HashSet<String>,
}

#[derive(Message, Serialize, Deserialize)]
#[rtype(result = "Result<(), TopicFilterError>")]
pub struct Subscribe {
    pub topics: Vec<String>,
    pub session_id: String,
    pub account_id: String,
}

#[derive(Message)]
#[rtype(result = "Result<(), String>")]
pub struct RegisterAccount(pub Subscribe);

#[derive(Message, Serialize, Deserialize)]
#[rtype(result = "()")]
pub struct Unsubscribe {
    pub topics: Vec<String>,
    pub session_id: String,
    pub account_id: String,
}

pub struct WsServer {
    sessions: HashMap<String, Recipient<Message>>,
    subscriptions: HashMap<String, HashMap<String, HashMap<String, TopicFilter>>>,
    visitor_count: Arc<AtomicUsize>,
    pulsar: Pulsar<TokioExecutor>,
}

impl WsServer {
    pub fn new(visitor_count: Arc<AtomicUsize>, pulsar: Pulsar<TokioExecutor>) -> WsServer {
        WsServer {
            sessions: HashMap::new(),
            subscriptions: HashMap::new(),
            visitor_count,
            pulsar,
        }
    }

    fn send_message(&self, session_id: &str, message: &str) {
        if let Some(addr) = self.sessions.get(session_id) {
            let _ = addr.do_send(Message::Text(message.to_owned()));
        }
    }
}

impl Actor for WsServer {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let topic = SETTINGS.broker.topic.to_owned();
        let pulsar = self.pulsar.clone();
        let addr = ctx.address();

        tokio::spawn(async move {
            let mut consumer: Consumer<BrokerMessage, _> = pulsar
                .consumer()
                .with_topic(topic)
                .with_subscription_type(SubType::Exclusive)
                .with_subscription(nanoid!())
                .build()
                .await
                .expect("Failed to build consumer");

            while let Some(msg) = consumer
                .try_next()
                .await
                .expect("Failed to consumer.try_next")
            {
                consumer.ack(&msg).await.expect("Failed to consumer.ack");

                let data = match msg.deserialize() {
                    Ok(data) => data,
                    Err(e) => {
                        error!("could not deserialize message: {:?}", e);
                        continue;
                    }
                };

                info!("handle {}", data.topic_name.as_str());

                match (data.topic_name.as_str(), data.event_type.as_str()) {
                    ("$SYS/api", "subscribed") => {
                        let sub = match serde_json::from_value(data.data) {
                            Ok(sub) => sub,
                            Err(e) => {
                                error!("could not deserialize message: {:?}", e);
                                continue;
                            }
                        };
                        addr.do_send(RegisterAccount(sub))
                    }
                    ("$SYS/api", "unsubscribed") => {
                        let sub = match serde_json::from_value::<Unsubscribe>(data.data) {
                            Ok(sub) => sub,
                            Err(e) => {
                                error!("could not deserialize message: {:?}", e);
                                continue;
                            }
                        };
                        addr.do_send(sub)
                    }
                    _ => addr.do_send(data),
                };
            }
        });
    }
}

impl Handler<Connect> for WsServer {
    type Result = String;

    fn handle(&mut self, msg: Connect, _: &mut Context<Self>) -> Self::Result {
        let id = nanoid!();
        self.sessions.insert(id.to_owned(), msg.addr);
        self.visitor_count.fetch_add(1, Ordering::SeqCst);

        id
    }
}

impl Handler<Disconnect> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) {
        self.sessions.remove(&msg.id);

        for account_id in msg.account_ids {
            let len = self
                .subscriptions
                .get_mut(&account_id)
                .map(|sessions| {
                    sessions.remove(&msg.id);

                    sessions.keys().len()
                })
                .unwrap_or(0);

            if len == 0 {
                self.subscriptions.remove(&account_id);
            }
        }

        self.visitor_count.fetch_sub(1, Ordering::SeqCst);
    }
}

impl Handler<RegisterAccount> for WsServer {
    type Result = Result<(), String>;

    fn handle(&mut self, msg: RegisterAccount, _ctx: &mut Context<Self>) -> Self::Result {
        let _ = match self.sessions.get(&msg.0.session_id) {
            Some(s) => s.do_send(Message::Account(msg.0)),
            _ => return Err("session not found".to_owned()),
        };

        Ok(())
    }
}

impl Handler<Subscribe> for WsServer {
    type Result = Result<(), TopicFilterError>;

    fn handle(&mut self, msg: Subscribe, _ctx: &mut Context<Self>) -> Self::Result {
        if !self.sessions.contains_key(&msg.session_id) {
            return Err(TopicFilterError("session not found".to_owned()));
        }

        for topic in msg.topics {
            self.subscriptions
                .entry(msg.account_id.to_owned())
                .or_insert_with(HashMap::new)
                .entry(msg.session_id.to_owned())
                .or_insert_with(HashMap::new)
                .insert(topic.to_owned(), TopicFilter::new(topic)?);
        }

        Ok(())
    }
}

impl Handler<Unsubscribe> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: Unsubscribe, _: &mut Context<Self>) {
        let subscription = match self.subscriptions.get_mut(&msg.account_id) {
            Some(s) => s,
            _ => return,
        };

        let session = match subscription.get_mut(&msg.session_id) {
            Some(s) => s,
            _ => return,
        };

        for topic in msg.topics {
            session.remove(&topic);
        }

        if session.is_empty() {
            subscription.remove(&msg.session_id);
        }

        if subscription.is_empty() {
            self.subscriptions.remove(&msg.account_id);
        }
    }
}

impl Handler<BrokerMessage> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: BrokerMessage, _: &mut Context<Self>) -> Self::Result {
        let topic_name = match TopicNameRef::new(&msg.topic_name) {
            Ok(topic_name) => topic_name,
            Err(e) => {
                error!(
                    "could not TopicNameRef::new WsServer.BrokerMessage: {:?}",
                    e
                );

                return;
            }
        };

        let subscriptions = match msg.account_id.as_ref() {
            Some(account_id) => self
                .subscriptions
                .get(account_id)
                .map(|sessions| vec![sessions]),
            None => Some(self.subscriptions.values().collect()),
        };

        let subscriptions = match subscriptions {
            Some(subscriptions) => subscriptions,
            None => return,
        };

        let json_data = match serde_json::to_string(&msg) {
            Ok(data) => data,
            Err(e) => {
                error!(
                    "could not serde_json::to_string WsServer.BrokerMessage: {:?}",
                    e
                );

                return;
            }
        };

        for sessions in subscriptions {
            for (session_id, filters) in sessions {
                let matched = filters
                    .iter()
                    .any(|(_, filter)| filter.get_matcher().is_match(topic_name));

                if matched {
                    self.send_message(session_id, &format!("event {json_data}"))
                }
            }
        }
    }
}

pub async fn route(
    req: HttpRequest,
    stream: web::Payload,
    srv: web::Data<Addr<WsServer>>,
) -> Result<HttpResponse, Error> {
    ws::start(
        WsSession {
            id: "".to_owned(),
            hb: Instant::now(),
            addr: srv.get_ref().clone(),
            account_ids: HashSet::new(),
        },
        &req,
        stream,
    )
}
