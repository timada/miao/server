use actix_web::error::{ErrorBadRequest, ErrorUnauthorized};
use actix_web::{delete, post, web, Error, FromRequest, HttpMessage, HttpResponse, Result, Scope};
use futures_util::future::{err, ok, Ready};
use josekit::jwt::JwtPayload;
use josekit::{
    jws::HS256,
    jwt::{self},
};
use pulsar::{Producer, TokioExecutor};
use serde::Deserialize;
use std::ops::Deref;
use std::sync::{Arc, Mutex};
use std::time::SystemTime;

use crate::broker::BrokerMessage;
use crate::error::MiaoError;
use crate::settings::SETTINGS;
use crate::topic_filter::TopicFilterRef;
use crate::topic_name::TopicNameRef;
use crate::ws_server::{Subscribe, Unsubscribe};

struct User(pub String);

impl FromRequest for User {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(
        req: &actix_web::HttpRequest,
        _payload: &mut actix_web::dev::Payload,
    ) -> Self::Future {
        let extensions = req.extensions();
        let account_id = match extensions
            .get::<JwtPayload>()
            .and_then(|payload| payload.subject())
        {
            Some(v) => v,
            _ => return err(ErrorUnauthorized("unauthorized")),
        };

        ok(Self(account_id.into()))
    }
}

struct Token(pub String);

impl FromRequest for Token {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(
        req: &actix_web::HttpRequest,
        _payload: &mut actix_web::dev::Payload,
    ) -> Self::Future {
        let token = match req
            .headers()
            .get("x-miao-token")
            .and_then(|v| v.to_str().ok())
        {
            Some(v) => v,
            _ => return err(ErrorBadRequest("x-miao-token is missing fro header")),
        };

        let (payload, _) = match HS256
            .verifier_from_bytes(&SETTINGS.jwt.key)
            .and_then(|v| jwt::decode_with_verifier(&token, &v))
        {
            Ok(res) => res,
            _ => return err(ErrorBadRequest("x-miao-token is invalid")),
        };

        let expired = payload
            .expires_at()
            .map(|expires_at| expires_at.lt(&SystemTime::now()))
            .unwrap_or(false);

        if expired {
            return err(ErrorBadRequest("x-miao-token is invalid"));
        }

        let session_id = match payload.subject() {
            Some(v) => v,
            _ => return err(ErrorBadRequest("x-miao-token is invalid")),
        };

        ok(Self(session_id.into()))
    }
}

#[post("/publish")]
async fn publish(
    producer: web::Data<Arc<Mutex<Producer<TokioExecutor>>>>,
    message: web::Json<BrokerMessage>,
) -> Result<HttpResponse, MiaoError> {
    TopicNameRef::new(&message.topic_name).map_err(|e| MiaoError::BadRequest(e.to_string()))?;

    let _ = producer.lock().unwrap().send(message.deref()).await?;

    Ok(HttpResponse::Ok().json(serde_json::json!({"success": true})))
}

#[derive(Deserialize)]
struct SubscribePayload {
    pub topic_filters: Vec<String>,
}

#[post("/subscribe")]
async fn subscribe(
    producer: web::Data<Arc<Mutex<Producer<TokioExecutor>>>>,
    payload: web::Json<SubscribePayload>,
    user: User,
    token: Token,
) -> Result<HttpResponse, MiaoError> {
    let invalid_filters = payload
        .topic_filters
        .clone()
        .into_iter()
        .filter(|topic| TopicFilterRef::new(topic).is_err())
        .collect::<Vec<String>>()
        .join(", ");

    if !invalid_filters.is_empty() {
        return MiaoError::BadRequest(format!(
            "invalid topic filters: {}",
            invalid_filters.to_owned()
        ))
        .into_response();
    }

    let data = serde_json::to_value(&Subscribe {
        topics: payload.topic_filters.to_owned(),
        account_id: user.0,
        session_id: token.0,
    })?;

    let message = BrokerMessage {
        topic_name: "$SYS/api".to_owned(),
        account_id: None,
        data,
        metadata: None,
        event_type: "subscribed".to_owned(),
    };

    let _ = producer.lock().unwrap().send(&message).await?;

    Ok(HttpResponse::Ok().json(serde_json::json!({"success": true})))
}

#[delete("/unsubscribe")]
async fn unsubscribe(
    producer: web::Data<Arc<Mutex<Producer<TokioExecutor>>>>,
    payload: web::Json<SubscribePayload>,
    user: User,
    token: Token,
) -> Result<HttpResponse, MiaoError> {
    let invalid_filters = payload
        .topic_filters
        .clone()
        .into_iter()
        .filter(|topic| TopicFilterRef::new(topic).is_err())
        .collect::<Vec<String>>()
        .join(", ");

    if !invalid_filters.is_empty() {
        return MiaoError::BadRequest(format!(
            "invalid topic filters: {}",
            invalid_filters.to_owned()
        ))
        .into_response();
    }

    let data = serde_json::to_value(&Unsubscribe {
        topics: payload.topic_filters.to_owned(),
        account_id: user.0,
        session_id: token.0,
    })?;

    let message = BrokerMessage {
        topic_name: "$SYS/api".to_owned(),
        account_id: None,
        data,
        metadata: None,
        event_type: "unsubscribed".to_owned(),
    };

    let _ = producer.lock().unwrap().send(&message).await?;

    Ok(HttpResponse::Ok().json(serde_json::json!({"success": true})))
}

pub fn scope() -> Scope {
    web::scope("/api")
        .service(publish)
        .service(subscribe)
        .service(unsubscribe)
}
