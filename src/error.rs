use actix::fut::{ready, Ready};
use actix_web::{
    dev::{Service, ServiceRequest, ServiceResponse, Transform},
    http::StatusCode,
    HttpMessage, HttpResponse, HttpResponseBuilder, ResponseError,
};
use futures_util::Future;
use josekit::jwt::JwtPayload;
use std::{pin::Pin, rc::Rc};
use thiserror::Error;
use tracing::Instrument;

#[derive(Error, Debug)]
pub enum MiaoError {
    #[error("internal server error")]
    InternalServerError(String),

    #[error("{0}")]
    BadRequest(String),
}

impl MiaoError {
    pub fn into_response(self) -> Result<HttpResponse, Self> {
        Err(self)
    }
}

impl ResponseError for MiaoError {
    fn status_code(&self) -> StatusCode {
        match *self {
            MiaoError::InternalServerError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            MiaoError::BadRequest(_) => StatusCode::BAD_REQUEST,
        }
    }

    fn error_response(&self) -> HttpResponse {
        let mut res = HttpResponseBuilder::new(self.status_code());

        if let MiaoError::InternalServerError(e) = self {
            error!("{}", e);
        }

        res.json(
            serde_json::json!({"code": self.status_code().as_u16(), "message": self.to_string()}),
        )
    }
}

impl From<serde_json::Error> for MiaoError {
    fn from(e: serde_json::Error) -> Self {
        MiaoError::InternalServerError(e.to_string())
    }
}

impl From<pulsar::Error> for MiaoError {
    fn from(e: pulsar::Error) -> Self {
        MiaoError::InternalServerError(e.to_string())
    }
}

impl From<josekit::JoseError> for MiaoError {
    fn from(e: josekit::JoseError) -> Self {
        MiaoError::InternalServerError(e.to_string())
    }
}

pub struct MiaoActixError(String);

pub struct MiaoActix();

impl<S, B> Transform<S, ServiceRequest> for MiaoActix
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error> + 'static,
    B: 'static,
    S::Future: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    type Transform = MiaoActixMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(MiaoActixMiddleware {
            service: Rc::new(service),
        }))
    }
}

type BoxFuture<O> = Pin<Box<dyn Future<Output = O>>>;

pub struct MiaoActixMiddleware<S> {
    service: Rc<S>,
}

impl<S, B> Service<ServiceRequest> for MiaoActixMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error> + 'static,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    type Future = BoxFuture<Result<Self::Response, Self::Error>>;

    actix_web::dev::forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let service = Rc::clone(&self.service);
        let extensions = req.extensions();

        let account_id = extensions
            .get::<JwtPayload>()
            .and_then(|payload| payload.subject())
            .unwrap_or("anonyme");


        let req_span = error_span!("api", account_id, path = req.path());

        drop(extensions);

        Box::pin(async move { service.call(req).await }.instrument(req_span))
    }
}
