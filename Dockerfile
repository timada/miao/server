FROM gcr.io/distroless/cc

COPY ./target/release/miao-server /

EXPOSE 6750

CMD ["./miao-server"]
